let checkbox = document.querySelector('input[name=data-theme]');
const counters = document.querySelectorAll('.counter');
const speed = 300;

//<h2 class="followers-number" data-target="1987">0</h2>


checkbox.addEventListener('change', function() {
    if (this.checked) {
        document.documentElement.setAttribute('data-theme', 'light')       
    }else {
        document.documentElement.setAttribute('data-theme', 'dark')
    }
});


counters.forEach( counter => {
    const updateCount = () => {
        const target = +counter.getAttribute('data-target');
        const count = +counter.innerText;

        const inc = target / speed;

        if (count < target) {
            counter.innerText = Math.ceil(count + inc); 
            // kFormatter(target); 
            setTimeout(updateCount, 1);            
        } else {
            counter.innerText = target;
        }
    }    
    updateCount();
});

// function kFormatter (num) {
//     if (num > 9990) {
//         return (num/1000).toFixed(1) + 'k'
//     } else {
//         return num;
//     }
// }

